# Helm Chart for Java application using Tomcat

This Helm chart is designed to deploy a java application using a chart for Tomcat on Kubernetes. It includes manifests for Ingress, Service, Deployment, ConfigMap (used to generate application.propertie file and to specific configuration for tomcat if needed), ExternalSecret, allowing you to easily manage and deploy your Java application.

## Table of Contents

- [Manifests](#manifests)
  - [Ingress](#ingress)
  - [Service](#service)
  - [Deployment](#deployment)
  - [ConfigMap](#configmap)
  - [ExternalSecret](#externalsecret)
- [Extra environment variables](#extraenvironmentvariables)
---

## Manifests

### Ingress

The Ingress manifest (`ingress.yaml`) defines the rules for routing external traffic to your Java application. You can customize the following aspects:

- `ingress.annotations`: Additional annotations for the Ingress resource.
- `ingress.className`: The class to be used for this Ingress.
- `ingress.domainSuffix`: The domain suffix for your application to create the host value using the application.name + domainSuffiz. (This value is harcoded by ArgoCD when using CaaS).
- `ingress.host.enabled`: Enable/disable to add a hostname for your application (For CaaS the host is "{{ .Values.application.name }}.{{ .Values.ingress.domainSuffix }}").
- `ingress.contextPath`: The context path to access to your application.
- `ingress.pathType`: The path type for your Ingress.
- `ingress.tls.enabled`: Enable/disable TLS for your Ingress.
- `ingress.tls.tlsSecretName`: The name of the TLS secret (This value is harcoded by ArgoCD when using CaaS). 

### Service

The Service manifest (`service.yaml`) exposes your Java application within the Kubernetes cluster. Customize the following:

- `service.port`: The port for your service.
- `service.targetPort`: The target port for your service.

### Deployment

The Deployment manifest (`deployment.yaml`) specifies how your Java application should be deployed. Customize:

- `application.name`: The name of your Java application.
- `replicaCount`: The number of replicas for your application.
- `image.registry`: The Docker image registry for your application (This value is harcoded by ArgoCD when using CaaS). The value needs to have the '/' at the end and this field is used together with the 'image.repositoryPath' field and 'image.tag' to create the fully URL of the application's docker image. The format is '{{ .Values.image.registry }}{{ .Values.image.repositoryPath }}:{{ .Values.image.tag }}' .
- `image.repositoryPath`: The 'image.repositoryPath' field is used together with the 'image.registry' field and 'image.tag' to create the fully URL of the application's docker image. The format is '{{ .Values.image.registry }}{{ .Values.image.repositoryPath }}:{{ .Values.image.tag }}'
- `image.tag`: The Docker image tag for your application. (For CaaS that parameter can be used in application.yaml file)
- `image.pullPolicy`: The image pull policy.
- `image.pullSecret`: The secret for pulling the Docker image (This value is harcoded by ArgoCD when using CaaS).
- `debug.enabled`: Enable/disable Java remote debugging.
- `debug.port`: The port for Java remote debugging.
- `probes.enabled`: Enable/disable readiness, liveness, and startup probes.
- `probes.readinessProbe`: Configuration for the readiness probe.
- `probes.livenessProbe`: Configuration for the liveness probe.
- `probes.startupProbe`: Configuration for the startup probe.

### ConfigMap - Spring Boot config

If Spring Property Config is enabled (springPropConfig.enable set to true), a ConfigMap manifest (`cm-app-properties.yaml`) is provided to create a ConfigMap from the application.properties file to be able to customize your propertie field by environments. 

To enabling this feature you will need to define in your values.yaml:

- `springPropConfig.enable`: Set to true to enable the creation of the config map.
- `springPropConfig.applicationPropertiesPath`: Path of the file content of the application.properties file (The path of the file to be loaded must be located in app-properties folder).

### ConfigMap - Tomcat files config

This configmap is used to edit the configuration files found in the "/usr/local/tomcat/conf" folder of the tomcat server.

If the `tomcatConfigFiles.enabled` variable is set to true, a ConfigMap manifest (`configmap-tomcat.yaml`) is provided to create a ConfigMap from some files that each project can customize across environments:

- `catalina.properties`
- `server.xml`
- `context.xml`

#### catalina.properties
The catalina.properties file in Tomcat is a configuration file that defines runtime properties for the Tomcat server, such as system properties and classpath settings, allowing customization and fine-tuning of the server's behavior

To customize this file you need to add the following values to your value.yaml:
- `tomcatConfigFiles.enabled` in true. This way helm chart will create the configmap
- `tomcatConfigFiles.catalinaProperties.enable`: Set to true to enable the creation of the catalina.properties file within the config map.
- `tomcatConfigFiles.catalinaProperties.path`: Path of the file content related to the custom catalina.properties file (The path of the file to be loaded must be located in tc-files folder).

#### server.xml
The `server.xml` file in Tomcat is a crucial configuration file that defines the server's settings, including ports, connectors, and virtual hosts, enabling customization and management of the Tomcat web server environment.

To customize this file you need to add the following values to your value.yaml:
- `tomcatConfigFiles.enabled` in true. This way helm chart will create the configmap
- `tomcatConfigFiles.serverXml.enable`: Set to true to enable the creation of the server.xml file within the config map.
- `tomcatConfigFiles.serverXml.path`: Path of the file content related to the custom server.xml file (The path of the file to be loaded must be located in tc-files folder).

#### context.xml
The context.xml file in Tomcat is used to configure individual web applications' settings, such as database connections, resource references, and context parameters, providing flexibility and customization at the application level within the Tomcat server environment.

To customize this file you need to add the following values to your value.yaml:
- `tomcatConfigFiles.enabled` in true. This way helm chart will create the configmap
- `tomcatConfigFiles.contextXml.enable`: Set to true to enable the creation of the context.xml within the config map.
- `tomcatConfigFiles.contextXml.path`: Path of the file content related to the custom context.xml file (The path of the file to be loaded must be located in tc-files folder).

#### web.xml
The `web.xml` file in Tomcat servers is a deployment descriptor used to configure web applications. It defines servlets, filters, and listeners, along with their mappings and initialization parameters, providing essential configuration for web application deployment.

To customize this file you need to add the following values to your value.yaml:
- `tomcatConfigFiles.enabled` in true. This way helm chart will create the configmap
- `tomcatConfigFiles.webXml.enable`: Set to true to enable the creation of the web.xml file within the config map.
- `tomcatConfigFiles.webXml.path`: Path of the file content related to the custom web.xml file (The path of the file to be loaded must be located in tc-files folder).

**If you need another file in this configmap, create a MergeRequest with the changes to be added in this helm chart or open a ticket to request to do it.**

### ExternalSecret

If external secrets are enabled (externalSecrets.enabled set to true), an External Secret manifest (`external-secret.yaml`) is provided to create a secret with sensitive values needed by the application and obtained from a secrets provider (in caas the provider and Hashicorp Vault).

Customize the following in the External Secret YAML:

- `externalSecrets.enabled`: Determines whether External Secrets are enabled (true to enable, false to disable).

- `externalSecrets.secretStoreRefName`: Specifies the name of the secret store reference. To use this helm chart in CaaS the value must be "vault-backend-customer"

- `externalSecrets.secretStoreRefKind`: Defines the type of the secret store reference. Most used kind is "SecretStore" (CaaS).

- `externalSecrets.secretData`: Specifies an array of secret configurations, each containing the following details:

  - `externalSecrets.secretData.secretKey`: The key used in the External Secret to identify the secret data (Key created in the Secret manifest result of this External Secret).

  - `externalSecrets.secretData.secretName`: The name of the External Secret, typically generated using a combination of the application name and "external-secrets"

  - `externalSecrets.secretData.remoteRefKey`: The path name of the secret manager where the fields declared in 'property' are the keys to retrieve the values.

  - `externalSecrets.secretData.property`: The property key name used in the secret manager to store the corresponding value.

  - `externalSecrets.secretData.envVarName`: The name of the environment variable to be created dynamically in the deployment.yaml, allowing the application to use the secret value.



## Extra environment variables

If necessary, this Helm chart provides the flexibility to incorporate additional environment variables for consumption in your application. Simply include the following configuration in the values.yaml file:

```yaml
# my-values.yaml
environmentVariables:
  - name: VAR1
    value: "custom_value1"
  - name: VAR3
    value: "new_variable"
```

This code dynamically generates environment variables within the deployment.yaml manifest, and once the deployment process is complete, you'll find these variables accessible within the pod.